<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

include_once dirname(__FILE__) . '/includes/bootstrap.php';

$showRightColumn = $this->countModules('position-7') > 0 || $this->countModules('component-center-right') > 0;
$showLeftColumn  = $this->countModules('position-8') > 0;
$showSingleSidebar = (true === $showRightColumn && false === $showLeftColumn) || (false === $showRightColumn && true === $showLeftColumn);
$showNoColumns = false === $showRightColumn && false === $showLeftColumn;
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
  <?php include_once dirname(__FILE__) . '/includes/head.php'; ?>
	<body <?php if ($itemId): ?> id="item<?php echo $itemId; ?>" <?php endif; ?> class="bg-grey-lightest font-inter font-normal antialiased text-black leading-normal <?=$bodyclass?>">
    <!-- Menu -->
    <?php if ($this->countModules('nav-top')): ?>
      <div id="nav-top" class="border-b-4 border-red">
        <jdoc:include type="modules" name="nav-top" style="standard"/>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('position-1')): ?>
      <div id="position-1" class="border-b-4 border-red">
        <jdoc:include type="modules" name="position-1" style="standard"/>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('header-top')): ?>
      <div id="header-top">
        <div class="wrapper mb-4 flex items-start justify-end flex-wrap">
          <jdoc:include type="modules" name="header-top" style="none" />
        </div>
      </div>
    <?php endif; ?>

    <div class="md:w-full md:max-w-3/4 md:mx-auto px-3 pb-6 md:px-0">
      <div class="lg:flex">
        <?php if ($this->countModules('position-8')): ?>
          <aside id="sidebar" class="hidden z-90 top-16 bg-grey-darker w-full border-b lg:-mb-0 lg:static lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
            <div class="lg:block lg:relative lg:sticky lg:top-16">
              <div id="position-8" class="px-6 pt-6 overflow-y-auto text-base lg:text-sm  sticky?lg:h-(screen-16)">
                <jdoc:include type="modules" name="position-8" style="sidebarMenu" />
              </div>
            </div>
          </aside>
        <?php endif; ?>
        <?php if ($showNoColumns) : ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible">
        <?php elseif ($showSingleSidebar) : ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-3/4 xl:w-4/5">
        <?php else: ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-2/4 xl:w-3/5">
        <?php endif; ?>
          <div id="content">
            <?php if ($this->countModules('position-0')): ?>
              <div id="position-0" class="flex justify-end p-4">
                <jdoc:include type="modules" name="position-0" style="standard"/>
              </div>
            <?php endif; ?>
            <div class="w-full">
              <?php if ($this->countModules('position-3')): ?>
                <jdoc:include type="modules" name="position-3" style="xhtml" />
              <?php endif; ?>
              <jdoc:include type="message" />
              <jdoc:include type="component" />
              <?php if ($this->countModules('position-2')): ?>
                <jdoc:include type="modules" name="position-2" style="xhtml" />
              <?php endif; ?>
              <?php if ($this->countModules('component-center-bottom')): ?>
                <jdoc:include type="modules" name="component-center-bottom" style="xhtml" />
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?php if ($showRightColumn) : ?>
            <aside id="right" class="z-90 top-16 w-full border-b lg:-mb-0 lg:static lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
              <div class="lg:block lg:relative lg:sticky lg:top-16">
                <div class="px-6 pt-6 overflow-y-auto text-base lg:text-sm lg:pl-6 lg:pr-8 sticky?lg:h-(screen-16)">
                  <?php if ($this->countModules('position-7')) : ?>
                    <jdoc:include type="modules" name="position-7" style="sidebarMenu" />
                  <?php endif; ?>
                  <?php if ($this->countModules('component-center-right')) : ?>
                    <jdoc:include type="modules" name="component-center-right" style="sidebarMenu" />
                  <?php endif; ?>
                </div>
              </div>
            </aside><!-- end right -->
        <?php endif ?>
      </div>
    </div>
    <div id="pre-footer" class="bg-red text-grey-light py-3">
      <div class="wrapper">
        <button type="button" id="back-to-top" class="float-right hover:text-blue">Back to top</button>
        <p>&copy; <?php echo date('Y'); ?> <?php echo $sitename; ?></p>
      </div>
    </div>
    <div id="footer" class="bg-blue-darker text-grey py-6 leading-relaxed">
        <?php if ($this->countModules('footer-top')) : ?>
          <div id="footer-top">
            <div class="wrapper flex items-start justify-end flex-wrap">
              <jdoc:include type="modules" name="footer-top" style="xhtml" />
            </div>
          </div>
        <?php endif; ?>
        <jdoc:include type="modules" name="footer" style="none" />
        <?php if ($this->countModules('footer-bottom')) : ?>
          <jdoc:include type="modules" name="footer-bottom" style="none" />
        <?php endif; ?>
    </div>
		<jdoc:include type="modules" name="debug" style="none" />
	</body>
</html>