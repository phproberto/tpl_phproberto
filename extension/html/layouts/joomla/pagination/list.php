<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$list = $displayData['list'];
?>
<ul class="flex pagination">
	<li class="pagination-start pagination__item"><?php echo $list['start']['data']; ?></li>
	<li class="pagination-prev pagination__item ml-4"><?php echo $list['previous']['data']; ?></li>
	<?php foreach ($list['pages'] as $page) : ?>
		<li class="pagination__item ml-4 hidden md:inline-block"><?=$page['data']?></li>
	<?php endforeach; ?>
	<li class="pagination-next pagination__item ml-4"><?php echo $list['next']['data']; ?></li>
	<li class="pagination-end pagination__item ml-4"><?php echo $list['end']['data']; ?></li>
</ul>
