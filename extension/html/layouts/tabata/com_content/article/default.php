<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Layout\LayoutHelper;

JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');
HTMLHelper::addIncludePath(JPATH_SITE . '/components/com_content/helpers/html');

/**
 * Layout variables
 *
 * @var  stdClass   $article  Article as it comes from joomla models/helpers
 */
extract($displayData);

$params = $article->params;
$canEdit = $article->params->get('access-edit');
$info	= $params->get('info_block_position', 0);

$link = Route::_(
	ContentHelperRoute::getArticleRoute(
		$article->slug, $article->catid, $article->language
	)
);

$categoryLink = Route::_(
		ContentHelperRoute::getCategoryRoute($article->catslug
	)
);

$showTags = $info == 0 && $params->get('show_tags', 1) && !empty($article->tags->itemTags);
?>
<div class="w-full mb-6">
	<div class="flex flex-col justify-between leading-loose text-2xl">
		<?php echo LayoutHelper::render('tabata.com_content.article.image', ['article' => $article, 'options' => ['class' => 'w-full']]); ?>
		<div class="mb-2">
			<div class="text-black text-5xl mb-5 xs:float-left lg:float-none">
				<a class="text-grey-darkest hover:underline" href="<?php echo $link ?>" itemprop="url">
					<?php echo $this->escape($article->title); ?>
				</a>
			</div>
			<div class="text-grey-darkest text-xl">
				<?php echo $article->text; ?>
				<?php echo $article->event->afterDisplayContent; ?>
			</div>
		</div>
		<?php if ($showTags) : ?>
			<div class="py-4">
				<div class="">
					<?php echo LayoutHelper::render('tabata.com_content.article.tags', ['article' => $article]); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>
