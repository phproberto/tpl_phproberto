<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_search
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;

if ($width)
{
	$moduleclass_sfx .= ' ' . 'mod_search' . $module->id;
	$css = 'div.mod_search' . $module->id . ' input[type="search"]{ width:auto; }';
	Factory::getDocument()->addStyleDeclaration($css);
	$width = ' size="' . $width . '"';
}
else
{
	$width = '';
}

$value = Factory::getApplication()->input->getString('searchword');

$showButton     = $params->get('button', 0);
$imageButton    = $params->get('imagebutton', 0);
$buttonLabel    = htmlspecialchars($params->get('label', JText::_('MOD_SEARCH_LABEL_TEXT')), ENT_COMPAT, 'UTF-8');
$buttonPosition = $params->get('button_pos', 'left');
$buttonText     = htmlspecialchars($params->get('button_text', Text::_('MOD_SEARCH_SEARCHBUTTON_TEXT')), ENT_COMPAT, 'UTF-8');
?>
<div class="search<?php echo $moduleclass_sfx; ?>">
	<form action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-inline" role="search">
		<div class="flex flex-wrap">
			<div class="search__box">
				<label for="mod-search-searchword<?=$module->id?>" class="hidden"><?=$buttonLabel?></label>
				<input name="searchword" id="mod-search-searchword<?=$module->id?>" maxlength="<?=$maxlength?>"
					class="w-full bg-grey-lighter align-middle appearance-none border rounded border-grey-light w-full py-2 px-3 text-grey-darker focus:outline-none focus:bg-white focus:shadow-outline"
					value="<?=$value?>"
					type="search"<?=$width?> placeholder="<?=$text?>" />
			</div>
			<?php if ($showButton) : ?>
				<button type="submit" class="ml-1 button"><?=$buttonText?></button>
			<?php endif; ?>
		</div>
		<input type="hidden" name="task" value="search" />
		<input type="hidden" name="option" value="com_search" />
		<input type="hidden" name="Itemid" value="<?php echo $mitemid; ?>" />
	</form>
</div>
