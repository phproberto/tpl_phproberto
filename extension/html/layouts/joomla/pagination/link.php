<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

/** @var JPaginationObject $item */
$item = $displayData['data'];

?>
<?php if ($displayData['active']) : ?>
	<a class="pagenav" title="<?=$item->text?>" href="<?=$item->link?>"><?=$item->text?></a>
<?php else : ?>
	<span class="pagenav"><?=$item->text?></span>
<?php endif;
