<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$displayData = isset($displayData) ? $displayData : [];

/**
 * Layout variables
 *
 * @var   string  $class   Classes to apply. Separated by spaces
 * @var   string  $width   Width to apply to the SVG
 * @var   string  $height  Height to apply to the SVG
 */
extract($displayData);

$class = isset($class) ? $class : 'fill-current h-9 w-9';
$width = isset($width) ? $width : '44';
$height = isset($height) ? $height : '44';
?>
<svg class="<?=$class?>" width="<?=$width?>" height="<?=$height?>" version="1.0" xmlns="http://www.w3.org/2000/svg"
 viewBox="0 0 150.000000 150.000000"
 preserveAspectRatio="xMidYMid meet">
<metadata></metadata>
<g transform="translate(0.000000,150.000000) scale(0.100000,-0.100000)"
fill="#000000" stroke="none">
<path d="M955 1147 c-62 -15 -105 -37 -105 -52 0 -45 47 -85 65 -55 3 6 39 10
80 10 70 0 75 -1 94 -29 35 -49 22 -98 -45 -161 -59 -57 -94 -118 -94 -167 0
-37 3 -43 24 -49 52 -13 70 2 80 66 4 25 24 54 71 104 76 81 95 114 95 170 0
75 -56 141 -138 163 -54 14 -72 14 -127 0z"/>
<path d="M720 951 c-8 -5 -26 -13 -40 -19 -40 -16 -117 -49 -202 -86 -18 -8
-53 -22 -78 -32 -74 -29 -90 -43 -90 -79 0 -18 7 -38 18 -46 16 -14 30 -21 92
-47 14 -5 46 -19 72 -31 26 -12 50 -21 53 -21 3 0 25 -9 50 -20 25 -11 65 -29
90 -40 25 -11 51 -20 58 -20 22 0 37 22 37 55 0 25 -6 33 -32 44 -18 7 -52 22
-75 32 -24 10 -46 19 -51 19 -5 0 -26 8 -48 19 -21 10 -56 24 -76 32 -41 14
-48 26 -20 35 22 8 47 17 104 43 26 12 50 21 53 21 5 0 75 30 123 53 25 12 29
51 8 80 -14 19 -23 20 -46 8z"/>
<path d="M952 537 c-45 -47 -11 -127 53 -127 37 0 75 37 75 74 0 67 -83 101
-128 53z"/>
</g>
</svg>