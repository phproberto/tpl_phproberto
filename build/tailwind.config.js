let colors = {
  'transparent': 'transparent',

  'green-darker': '#69722B',
  'green-dark': '#8F9B39',
  'green': '#AEBC46',
  'green-light': '#C9D386',
  'green-lighter': '#E1E7BE',

  'black': '#22292f',
  'grey-darkest': '#3d4852',
  'grey-darker': '#606f7b',
  'grey-dark': '#7A7A7A',
  'grey': '#b8c2cc',
  'grey-light': '#dae1e7',
  'grey-lighter': '#F1F1F1',
  'grey-lightest': '#f8fafc',
  'white': '#ffffff',

  'red-darkest': '#3b0d0c',
  'red-darker': '#621b18',
  'red-dark': '#cc1f1a',
  'red': '#DA4327',
  'red-light': '#ef5753',
  'red-lighter': '#f9acaa',
  'red-lightest': '#fcebea',

  'orange-darkest': '#462a16',
  'orange-darker': '#613b1f',
  'orange-dark': '#de751f', 
  'orange': '#F06229',
  'orange-light': '#faad63',
  'orange-lighter': '#fcd9b6',
  'orange-lightest': '#fff5eb',

  'blue-darker': '#183A55',
  'blue-dark': '#206F9E',
  'blue': '#3897CB',
  'blue-light': '#7BB7DC',
  'blue-lighter': '#BFDFDC',

  'yellow': '#F1AA20'
}

const fontFamily = {
  inter: [
    'Inter',
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    '"Noto Sans"',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
    '"Noto Color Emoji"',
  ],
  sans: [
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    'Roboto',
    '"Helvetica Neue"',
    'Arial',
    '"Noto Sans"',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
    '"Noto Color Emoji"',
  ],
  serif: [
    'Georgia',
    'Cambria',
    '"Times New Roman"',
    'Times',
    'serif',
  ],
  mono: [
    'Menlo',
    'Monaco',
    'Consolas',
    '"Liberation Mono"',
    '"Courier New"',
    'monospace',
  ],
};

const maxWidth = {
  xs: '20rem',
  sm: '24rem',
  md: '28rem',
  lg: '32rem',
  xl: '36rem',
  '2xl': '42rem',
  '3xl': '48rem',
  '4xl': '56rem',
  '5xl': '64rem',
  '6xl': '72rem',
  full: '100%',
  '1/4': '25%',
  '1/2': '50%',
  '3/4': '75%'
}
module.exports = {
  theme: {
  	colors: colors,
    fontFamily: fontFamily,
    maxWidth: maxWidth
  },
  variants: {},
  plugins: [],
}
