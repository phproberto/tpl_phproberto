<?php
/**
 * @package	 Joomla.Site
 * @subpackage	com_content
 *
 * @copyright	 Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license	 GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Layout\LayoutHelper;

// Blog category
if ('blog' === $this->item->category_alias)
{
	echo LayoutHelper::render('tabata.com_content.article.blog', ['article' => $this->item]);

	return;
}

echo LayoutHelper::render('tabata.com_content.article.default', ['article' => $this->item]);
