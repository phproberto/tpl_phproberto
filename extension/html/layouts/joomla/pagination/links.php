<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

use Joomla\Registry\Registry;

$list = $displayData['list'];
$pages = $list['pages'];

?>

<ul class="flex pagination" style="text-align: center;">
	<?php if (!empty($pages)) : ?>
		<li class="pagination-start pagination__item"><?=JLayoutHelper::render('joomla.pagination.link', $pages['start']);?></li>
		<li class="pagination-prev pagination__item ml-4"><?=JLayoutHelper::render('joomla.pagination.link', $pages['previous']); ?></li>
		<?php foreach ($pages['pages'] as $k => $page) : ?>
			<li class="pagination__item ml-4 hidden md:inline-block"><?=JLayoutHelper::render('joomla.pagination.link', $page)?></li>
		<?php endforeach; ?>
		<li class="pagination-next pagination__item ml-4"><?=JLayoutHelper::render('joomla.pagination.link', $pages['next'])?></li>
		<li class="pagination-end pagination__item ml-4"><?=JLayoutHelper::render('joomla.pagination.link', $pages['end'])?></li>
	<?php endif; ?>
</ul>
