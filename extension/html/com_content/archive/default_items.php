<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Layout\LayoutHelper;
?>
<div id="archive-items">
	<?php foreach ($this->items as $item) : ?>
		<?php echo LayoutHelper::render('tabata.com_content.article.preview', ['article' => $item]); ?>
	<?php endforeach; ?>
</div>

