<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;

extract($displayData);
?>
<div class="pagination">
	<?php echo $pagination->getPagesLinks(); ?>
</div>
