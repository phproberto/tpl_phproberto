(function() {
  'use strict';

  function init() {
    // Let GC remove unneeded parts
    document.removeEventListener('DOMContentLoaded', init);

    prettyPrint();
  }

  document.addEventListener("DOMContentLoaded", init);
})();
