<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_languages
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="mod-languages<?php echo $moduleclass_sfx ?>">
<?php if ($headerText) : ?>
	<div class="pretext"><p><?php echo $headerText; ?></p></div>
<?php endif; ?>

<?php if ($params->get('dropdown', 1)) : ?>
	<form name="lang" method="post" action="<?php echo htmlspecialchars(JURI::current()); ?>">
	<select class="inputbox" onchange="document.location.replace(this.value);" >
	<?php foreach ($list as $language) : ?>
		<option dir=<?php echo JLanguage::getInstance($language->lang_code)->isRTL() ? '"rtl"' : '"ltr"'?> value="<?php echo $language->link;?>" <?php echo $language->active ? 'selected="selected"' : ''?>>
		<?php echo $language->title_native;?></option>
	<?php endforeach; ?>
	</select>
	</form>
<?php else : ?>
	<ul class="flex w-full justify-end mx-auto<?php echo $params->get('inline', 1) ? 'lang-inline' : 'lang-block';?>">
	<?php foreach ($list as $i => $language) : ?>
		<?php if ($params->get('show_active', 0) || !$language->active):?>
			<?php
				$title = sprintf(JText::_('TPL_TABATA_LBL_SWITCH_LANGUAGE_TO'), $language->title_native);
			?>
			<li class="<?php echo $i !== count($list) - 1 ? 'mr-2' : '' ;?>" dir="<?php echo JLanguage::getInstance($language->lang_code)->isRTL() ? 'rtl' : 'ltr' ?>">
				<?php if ($language->active) : ?>
					<a class="bg-red text-white pt-1 pb-2 px-3 hover:bg-red rounded-bl rounded-br" href="<?=$language->link?>" title="<?=$title?>">
				<?php else : ?>
					<a class="bg-blue-darker text-white pt-1 pb-2 px-3 hover:bg-blue rounded-bl rounded-br" href="<?=$language->link?>" title="<?=$title?>">
				<?php endif; ?>
				<?php if ($params->get('image', 1)):?>
					<?php echo JHtml::_('image', 'mod_languages/' . $language->image . '.gif', $language->title_native, array('title' => $language->title_native), true);?>
				<?php else : ?>
					<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef);?>
				<?php endif; ?>
				</a>
			</li>
		<?php endif;?>
	<?php endforeach;?>
	</ul>
<?php endif; ?>

<?php if ($footerText) : ?>
	<div class="posttext"><p><?php echo $footerText; ?></p></div>
<?php endif; ?>
</div>
